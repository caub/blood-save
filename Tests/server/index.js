process.env.PORT = 3001;
const assert = require('assert');
const MongoClient = require('mongodb');
const fetch = require('node-fetch');
const {DB_URL} = require('../../Code/server/config');
const server = require('../../Code/server');
const socket = require('socket.io-client')(`http://localhost:${process.env.PORT}`);

// fetch util
const toJson = r => (/^application\/json/.test(r.headers.get('Content-Type')) ? r.json() : r.text())
	.then(d => Promise[r.status>=400?'reject':'resolve'](d));

const fetchJSON = (url, {method = 'GET', body}={}) => fetch(`http://localhost:${process.env.PORT}${url}`, {
	method,
	body: body && JSON.stringify(body), 
	headers: {'Content-Type': 'application/json', Accept:'*/json'}
}).then(toJson);


before(async () => {
	const db = await MongoClient.connect(DB_URL);
	await db.collection('users').deleteMany();
	await new Promise(resolve => {
		socket.on('connect', () => {
			console.log('test socket connected');
			resolve();
		});
	});
});

after(() => {
	server.close();
});


describe('server socket API', () => {

	let privId;
	let id;

	describe('Create data', () => {

		it('should validate missing address', () => {

			return fetchJSON('/details', {
				method: 'POST', 
				body: {firstName: 'joe', lastName: 'doe', email:'j.d@x.y', tel:'123', type:'O-', address:'iijj', lng: '9.3', lt: '9.3'}
			}).catch(e => {
				assert(e.message.includes('lat'));
			});
		});

		it('should create one post, and be notified by socket', done => {
			let evtCount = 0;

			socket.once('details new', data => {
				assert(!data.ip);
				assert.deepEqual(data.coords, [9.3, 9.3]);
				if (++evtCount >= 2) done();
			});

			fetchJSON('/details', {
				method: 'POST', 
				body: {firstName: 'joe', lastName: 'doe', email:'j.d@x.y', tel:'123', type:'O-', address:'12 Hobo Bd, Qux', lng: '9.3', lat: '9.3'}
			}).then(r => {
				assert(r.privId);
				assert.deepEqual(r.coords, [9.3, 9.3]);
				privId = r.privId;
				id = r._id;
				if (++evtCount >= 2) done();
			});
		});

		it('should create another post', () => {

			return fetchJSON('/details', {
				method: 'POST', 
				body: {firstName: 'jane', lastName: 'doe', email:'k.d@x.y', tel:'124', type:'AB', address:'12 Hobo Bd, Qux', lng: '9.4', lat: '8.3'}
			}).then(r => {
				assert(r.privId);
				assert.deepEqual(r.coords, [9.4, 8.3]);
			});
		});
	});

	describe('Search data', () => {
		it('should warn about invalid query data', done => {
			socket.once('app error', e => {
				assert(e.message.includes('box'));
				done();
			});
			
			socket.emit('search', {bb: 'foo'});
		});

		it('should not find any match', done => {
			socket.once('search', data => {
				assert.equal(data.length, 0);
				done();
			});
			
			socket.emit('search', {bb: '7,43,7.01,43.01'});
		});

		it('should find 2 matches', done => {
			socket.once('search', data => {
				assert.equal(data.length, 2);
				assert(!data[0].email); // email not visible yet
				done();
			});
			
			socket.emit('search', {bb: '8.1 8.2 9.9 9.31'});
		});

		it('should find 1 match for this blood group', done => {
			socket.once('search', data => {
				assert.equal(data.length, 1);
				assert.equal(data[0].type, 'O-');
				done();
			});
			
			socket.emit('search', {bb: '8.1 8.5 9.9 9.3', type: 'O'});
		});
	});

	describe('Show more info with manual user action', () => {
		it('should 404 with an inexisting _id', () => {
			return fetchJSON(`/details/${id}2/email`, {method: 'POST'})
				.catch(e => {
					assert(e);
				});
		});
		it('should reveal email', () => {
			return fetchJSON(`/details/${id}/email`, {method: 'POST'})
				.then(email => {
					assert.equal(email, 'j.d@x.y');
				});
		});
		it('should reveal tel', () => {
			return fetchJSON(`/details/${id}/tel`, {method: 'POST'})
				.then(tel => {
					assert.equal(tel, '123');
				});
		});
	});


	describe('Manage own submission from private Id', () => {
		it('should retrieve donor data', () => {
			return fetchJSON('/details/' + privId)
				.then(r => {
					assert.equal(r.privId, privId);
				});
		});

		it('should edit donor data', done => {
			let evtCount = 0;

			socket.once('details new', data => {
				assert.equal(data.type, 'AB-');
				assert(!data.email); // it's filtered!
				if (++evtCount >= 2) done();
			});

			fetchJSON('/details/' + privId, {
				method: 'PATCH', 
				body: {email: 'john@z.foo', type: 'AB-'}
			}).then(r => {
				assert.equal(r.email, 'john@z.foo');
				assert.equal(r.type, 'AB-');
				if (++evtCount >= 2) done();
			});
		});

		it('should be able to delete donor data', done => {
			let evtCount = 0;

			socket.once('details delete', data => {
				if (++evtCount >= 2) done();
			});

			fetchJSON('/details/' + privId, {method: 'DELETE'})
				.then(() => fetchJSON('/details/' + privId))
				.catch(e => {
					assert(e);
					if (++evtCount >= 2) done();
				});
		});

	});
});