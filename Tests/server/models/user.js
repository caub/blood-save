const assert = require('assert');
const MongoClient = require('mongodb');
const {DB_URL} = require('../../../Code/server/config');

let User; // model


before(async () => {
	const db = await MongoClient.connect(DB_URL);
	User = await require('../../../Code/server/models/user')(db, {init: true, collectionName: 'users2'});
	await db.collection('users2').deleteMany();
	// use a different collection name, to avoid conflict with other parallel tests 
});

describe('server User model', () => {

	let privId;

	describe('Create data', () => {
		it('should validate missing email', async() => {
			await User.create({firstName: 'joe', lastName: 'doe', address: '12 pew St. NYC', tel:'123', type:'O-'})
				.catch(e => {
					assert(e.message.includes('email'));
				});
		});

		it('should validate missing address', async() => {
			await User.create({firstName: 'joe', lastName: 'doe', email:'j.d@x.y', tel:'123', type:'O-'})
				.catch(e => {
					assert(e.message.includes('address'));
				});
		});

		it('should validate missing position', async() => {
			await User.create({firstName: 'joe', lastName: 'doe', email:'j.d@x.y', tel:'123', type:'O-', address:'123 Foo St, Bar'})
				.catch(e => {
					assert(e.message.includes('lat'));
				});
		});


		it('should create donors', async() => {
			const v = await User.create({firstName: 'joe', lastName: 'doe', email:'j.d@x.y', tel:'123', type:'O-', lng: '7.025', lat: '42.25', address:'123 Foo St, Bar'});
			assert(v);
			assert(v.privId);
			privId = v.privId;
			assert.deepEqual(v.coords, [7.025, 42.25]);
		});
	});


	describe('Search data', () => {
		it('should warn about invalid query data', async() => {
			await User.search({bb: 'true'})
				.catch(e => {
					assert(e.message.includes('box'));
				});
			
		});

		it('should not find any match', async() => {
			const r = await User.search({bb: '7,43,7.01,43.01'});
			assert.equal(r.length, 0);
		});

		it('should find one result', async() => {
			const r = await User.search({bb: '7,42.2,7.03,43.01'});
			assert.equal(r.length, 1);
		});

		it('should not match with this blood type', async() => {
			const r = await User.search({bb: '7,42.2,7.03,43.01', type: 'A'});
			assert.equal(r.length, 0);
		});

		it('should match with the same blood type', async() => {
			const r = await User.search({bb: '7,42.2,7.03,43.01', type: 'O-'});
			assert.equal(r.length, 1);
		});

		it('should match with the same blood group', async() => {
			const r = await User.search({bb: '7,42.2,7.03,43.01', type: 'O'});
			assert.equal(r.length, 1);
		});
	});

	describe('Manage own submission from private Id', () => {
		it('should retrieve donor data', async() => {
			const data = await User.show(privId);
			assert.equal(data.privId, privId);
		});

		it('should edit donor data', async() => {
			const data = await User.edit(privId, {email: 'john@x.y'});
			assert.equal(data.privId, privId);
			assert.equal(data.email, 'john@x.y');
		});

		it('should be able to delete donor data', async() => {
			const r = await User.delete(privId);
			await User.show(privId).catch(e => {
				assert.equal(e.status, 404);
			});
		});
	});

});
