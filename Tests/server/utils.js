const assert = require('assert');
const {parseTelPrefix, parseCoords} = require('../../Code/server/utils');

describe('server utils', () => {

	describe('telephone number parsing', () => {
		it('should be empty for an empty input', () => {
			assert.deepEqual(parseTelPrefix(), {});
		});
		it('should extract +xx xxxx', () => {
			assert.deepEqual(parseTelPrefix('+33 492911124'), {telPrefix: '+33', tel: '492911124'});
		});
		it('should extract and convert 00xx xxxx', () => {
			assert.deepEqual(parseTelPrefix('0033 492911124'), {telPrefix: '+33', tel: '492911124'});
		});
		it('should return no prefix else', () => {
			assert.deepEqual(parseTelPrefix('0492911124'), {tel: '0492911124'});
		});
	});


	describe('{lat, lng} parsing', () => {
		it('should convert strings', () => {
			assert.deepEqual(parseCoords({lng:'-9.36', lat:'-.001'}), {lng: -9.36, lat: -.001});
		});
		it('should returns empty when lng or lat is not a number', () => {
			assert.deepEqual(parseCoords({lng:'-9.a6', lat:'-.001'}), {});
		});
		it('should returns empty when lng or lat is out of range', () => {
			assert.deepEqual(parseCoords({lng:'-182.2', lat:'-.001'}), {});
		});
	});

});

