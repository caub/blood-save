import React from 'react';
import {shallow} from 'enzyme';
import {SidebarView} from '../../../Code/app/components/Sidebar';
import assert from  'assert';
import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({adapter: new Adapter()});

describe('app <SidebarView />', () => {

	it('should render 3 buttons', () => {
		const wrapper = shallow(<SidebarView />);
		assert.equal(wrapper.find('a').length, 2); // terms, contact
		assert.equal(wrapper.find('button').length, 1); // donate blood
	});

});