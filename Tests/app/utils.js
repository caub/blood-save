const assert = require('assert');
const {SetWithToggle} = require('../../Code/app/utils');

describe('app utils', () => {

	describe('SetWithToggle', () => {
		it('should toggle a value', () => {
			const set = new SetWithToggle([1,3,5]);
			assert.deepEqual(set.toggle(3), new Set([1,5]));
		});
		it('should toggle a value with a forced condition', () => {
			const set = new SetWithToggle([1,3,5]);
			assert.deepEqual(set.toggle(3, true), new Set([1,3,5]));
		});
	});

});
