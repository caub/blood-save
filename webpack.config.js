const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production';
console.log(`building ${isProd ? 'production' : 'dev'}...`);

module.exports = {
	entry: {
		app: [
			'./Code/app/index.js',
			'./Code/app/index.scss'
		],
		edit: [
			'./Code/server/edit-app.js',
			'./Code/server/edit-app.scss'
		]
	},
	output: {
		path: process.cwd() + '/dist',
		filename: isProd ? '[name].js?v=[chunkhash:4]' : '[name].js',
		// chunkFilename: '[chunkhash].js'
	},
	devServer: {
		hot: true,
		contentBase: ['dist', 'node_modules/leaflet/dist', 'node_modules/esri-leaflet-geocoder/dist'],
		port: 9000
	},
	devtool: !isProd && 'sourcemap',
	node: {
		fs: 'empty',
		child_process: 'empty'
	},
	resolve: {
		extensions: ['.js', '.jsx', '.json']
	},
	module: {
		rules: [{
			test: /\.jsx?$/,
			loader: 'babel-loader',
			exclude: /node_modules/
		}, {
			test: /\.(css|scss)$/,
			use: ExtractTextPlugin.extract({
				fallback: 'style-loader',
				use: [
					'css-loader', 
					{loader: 'resolve-url-loader', options:{fail: false}}, 
					'sass-loader',
					...(isProd ? [{loader: 'postcss-loader', options: { 
						plugins: [
							require('autoprefixer')(['last 2 versions', '> 5%']),
							require('cssnano')() 
						]}
					}] : [])
				]
			})
		}, {
			test: /\.(svg|png|jpg|gif|cur|ttf|eot|woff2?)(\?[a-z0-9]+)?$/,
			loader: 'file-loader?name=[path][name].[ext]?v=[hash:4]'
		}]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': isProd ? {
				'API_ORIGIN': JSON.stringify('blood-save.herokuapp.com')
			} : {}
		}),
		new ExtractTextPlugin(`[name].css${isProd ? '?v=[contenthash:4]' : ''}`),
		new HtmlWebpackPlugin({
			//hash: true,
			title: 'Blood Save',
			template: './Code/app/index.html',
			filename: './index.html',
			chunks: ['app']
		}),
		...(isProd ? [
			new CopyWebpackPlugin([
				{from: 'node_modules/leaflet/dist/'},
				{from: 'node_modules/esri-leaflet-geocoder/dist/'}
			]),
			new UglifyJSPlugin({
				uglifyOptions: {
					ie8: false,
					ecma: 8
				}
			})
			// new CompressionPlugin({
			// 	test: /\.(js|css|svg|png|jpg|gif|cur|ttf|eot|woff2?)/,
			// 	asset: '[path]', 
			// 	deleteOriginalAssets: true
			// })
		] : [])
	]
};