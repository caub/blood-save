# Blood Donation Map app

### Run it locally:

- have Node.JS 8.x (LTS in november 2017)
- have a MongoDB server (`mongod`) running
- install: `npm i`
- start: `npm start`

### APIs

REST:
```
GET    /details/{privId}: show (json/html) this donor post by its private id
POST   /details: create a new donor submission
	body: {lng, lat, address, firstName, lastName, email, tel, type}

PATCH  /details/{privId}
	body: {firstName, lastName, email, tel, type, trashed}

DELETE /details/{privId}

POST   /details/{id}/email: reveal email (using public _id field here)
POST   /details/{id}/tel: reveal phone number (using public _id field here)
```
socket pull:
```
'search'  {bb (, type)}: search donors in a boundingbox, blood type param is optional
```
socket push events:
```
'search'   (results of a search)
'details new' (notification broadcasted for a new or modified donor submission)
'details delete' (notification of a deletion (or temporary trashing))

```

### todos:
- [ ] add possible &donor=:id in frontend routing, and link to it in notification to show it.
- [ ] Use [proptype](https://www.npmjs.com/package/airbnb-prop-types) or [flow](https://flow.org/en/docs/getting-started)
- [ ] Allow a user to visualize its submission on the map (normally only one) at any time after he posted it, and move it (We need to do it in the map, not in a form, to keep address/lng/lat synchronized)


### heroku config

```
heroku config:set NPM_CONFIG_PRODUCTION=false
heroku config:set DB_URL=mongodb://...
heroku features:enable http-session-affinity
```