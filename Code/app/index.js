import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {createStore}  from 'redux';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import DonorMap from './components/DonorMap';
import SubmissionPopup from './components/SubmissionPopup';
import Notification from './components/Notification';
import reducers from './reducers.js';

const sp = new URLSearchParams(location.search); // polyfill.io it

const store = createStore(reducers, {
	mode: sp.has('post') ? 'post' : 'search',
	type: sp.has('type') ? new Set(sp.get('type').split(',')) : undefined
});

store.subscribe(() => {
	// update url
	const {mode, query = {}} = store.getState();
	let qs = `?${mode}`;
	if (query.bb) {
		qs += `&bb=${query.bb}`;
	}
	if (query.type) {
		qs += '&type=' + query.type;
	}
	if (history.state) {
		history.replaceState(history.state, '', qs);
	} else {
		history.pushState({mode, query}, '', qs);
	}
});

const App = () => (
	<Provider store={store}>
		<main>
			<Header />
			<DonorMap />
			<Sidebar />
			<SubmissionPopup />
			<Notification />
		</main>
	</Provider>
);

render(<App />, document.getElementById('root'));

// if (!Element.prototype.animate) {
// 	const s = document.createElement('script');
// 	s.src = 'https://unpkg.com/web-animations-js@2.3.1/web-animations.min.js';// todo host it ourselves
// 	document.body.appendChild(s);
// }