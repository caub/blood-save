// backend origin
export const origin = process.env.API_ORIGIN ? `https://${process.env.API_ORIGIN}` : 'http://localhost:3000';

// fetch util
const toJson = r => (/^application\/json/.test(r.headers.get('Content-Type')) ? r.json() : r.text())
	.then(d => Promise[r.status>=400?'reject':'resolve'](d));

export const fetchJSON = (url, {method = 'GET', body}={}) => fetch(origin + url, {
	method,
	body: body && JSON.stringify(body), 
	headers: {'Content-Type': 'application/json', Accept:'*/json'}
}).then(toJson);

export const DonorTemplate = data => `<div class="donor">
	<div><span>address</span><span class="address">${data.address}</span></div>
	<div><span>name</span><span>${data.firstName + ' ' + data.lastName}</span></div>
	<div><span>blood type</span><span>${data.type}</span></div>
	<div><span>email</span>${data.email ? `<a href="mailto:${data.email}" class="email">${data.email}</a>` : `<a href="javascript:void(0);" class="email">Click to show</a>`}</div>
	<div><span>phone</span>${data.tel ? `<a href="tel:${data.tel}" class="tel">${data.tel}</a>` : `<a href="javascript:void(0);" class="tel">Click to show</a>`}</div>
	<div><span>date</span><span>${new Date(data.created).toLocaleDateString()}</span></div>
</div>`;

export const PostTemplate = data => `<form class="donor-popup">
	<div class="address">
		${data}
	</div>
	<div>
		<span>Name</span>
		<input type="text" name="firstName" placeholder="First name" required>
		<input type="text" name="lastName" placeholder="Last name" required>
	</div>
	<div>
		<label for="email">Email</label>
		<input type="email" name="email" id="email" required>
	</div>
	<div>
		<label for="tel">Phone</label>
		<input type="tel" name="tel" id="tel" required>
	</div>
	<div>
		<span>Blood type</span>
		
		<input type="radio" name="type" id="btO" required><label for="btO">O</label>
		<input type="radio" name="type" id="btA" required><label for="btA">A</label>
		<input type="radio" name="type" id="btB" required><label for="btB">B</label>
		<input type="radio" name="type" id="btAB" required><label for="btAB">AB</label>
		
		<input type="radio" name="rh" id="rh1"><label for="rh1">-</label>
		<input type="radio" name="rh" id="rh2"><label for="rh2">+</label>
	</div>

	<input type="submit" value="Submit">

	<div class="error">
	</div>
</form>`;

export class SetWithToggle extends Set {
	toggle(value, force = !this.has(value)) {
		if (force) {
			this.add(value);
		} else {
			this.delete(value);
		}
		return this;
	}
};

export const bloodTypesToString = set => {
	const a = [];
	['O', 'A', 'B', 'AB'].forEach(group => {
		if (set.has(group+'-') && set.has(group+'+')) {
			a.push(group);
		} else if (set.has(group+'-')) {
			a.push(group+'-');
		} else if (set.has(group+'+')) {
			a.push(group+'+');
		}
	});
	return a.join(' ');
};