const {SetWithToggle} = require('./utils');

const actions = {
	toggleSidebar: state => ({...state, sidebarActive: !state.sidebarActive}),

	search: (state, {query}) => ({...state, query}),

	toggleMode: (state, {mode = state.mode === 'search' ? 'post' : 'search'} = {}) => ({...state, mode}),

	closeSidebar: state => ({...state, sidebarActive: false}),

	showSuccessPopup: (state, {data}) => ({...state, donorData: data}),

	closeSuccessPopup: state => ({...state, donorData: undefined}),

	toggleNotifications: state => ({...state, notificationActive: !state.notificationActive}),

	addNotification: (state, {data}) => ({...state, notifications: (state.notifications || []).concat(data)}),

	clearNotification: (state, {_id}) => ({...state, notifications: (state.notifications || []).filter(data => data._id !== _id)}),

	filterBloodType: ({bloodTypes = new Set(), ...state}, {bloodType, checked}) => {
		const bt = new SetWithToggle(bloodTypes);
		bt.toggle(bloodType, checked);

		// when blooType contain X- and X+ add X
		bt.toggle('O', bt.has('O-') && bt.has('O+'));
		bt.toggle('A', bt.has('A-') && bt.has('A+'));
		bt.toggle('B', bt.has('B-') && bt.has('B+'));
		bt.toggle('AB', bt.has('AB-') && bt.has('AB+'));

		return {...state, bloodTypes: bt};
	},
};

export default function (state = {}, action) {
	const fn = actions[action.type];
	if (fn) {
		return fn(state, action);
	}
	if (!/^@/.test(action.type)) console.warn(action, 'not impl');
	return state;
};