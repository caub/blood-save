import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';

const Div = styled.div`
	position: absolute;
	background: rgba(0,0,0,.5);
	z-index: 1002;
	top: 50px;
	bottom: 0;
	width: 300px;
	transform: translateX(${props => props.active ? '0' : '-300px'});
	transition: transform .3s linear;
	padding: 8px;
	display: flex;
	justify-content: center;
	flex-direction: column;
`;

export const SidebarView = ({sidebarActive, mode, onModeChange}) => (
	<Div active={sidebarActive}>

		<button className={'btn ' + (mode === 'post' ? 'active' : '')} 
			onClick={e => onModeChange(mode !== 'post')} 
			title="Click on the map to create your donor submission"
		>Be a Donor</button>

		<a href="/terms" className="btn">Terms</a>

		<a href="mailto:hello@blood.gv" className="btn">Contact us</a>

	</Div>
);

const mapStateToProps = (state, ownProps) => state; // todo tell react-redux to default to that

const mapDispatchToProps = (dispatch, ownProps) => ({
	onModeChange: donorAction => {
		dispatch({type: 'toggleMode'});
		if (donorAction) {
			dispatch({type: 'closeSidebar'});
		}
	}

});


const Sidebar = connect(mapStateToProps, mapDispatchToProps)(SidebarView);

export default Sidebar;