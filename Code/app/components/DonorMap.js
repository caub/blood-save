import React from 'react';
import {connect} from 'react-redux';
import L from 'leaflet';
import esri from 'esri-leaflet';
import geocoding from 'esri-leaflet-geocoder';
import io from 'socket.io-client';
import {DonorTemplate, PostTemplate, fetchJSON, origin} from '../utils';

class DonorMapView extends React.PureComponent {

	componentDidUpdate(p) {
		// comparing p.bloodTypes and this.props.bloodTypes depply isn't working well, todo
		// so will just store the previous bloodTypes (1) and compare refs
		if (this.bloodTypes !== this.props.bloodTypes) {
			this.bloodTypes = this.props.bloodTypes;
			this.searchResults.clearLayers();
			this.donors = new Map();
			this.search();
		}
		if (this.props.mode === 'search') {
			this.geocodeResults.clearLayers(); // clear post popup when switching back to search (todo check p.mode, of it's reliable with redux)
		}
	}

	componentDidMount() {
		document.body.classList.remove('loading');

		// init socket.io
		this.socket = io(origin);

		this.socket.on('connect', () => console.log('connected'));
		this.socket.on('disconnect', () => console.log('disconnected'));

		// init leaflet map
		this.map = L.map('map').setView([39, 0], 3);

		this.search = (e, cb = this.props.onSearch) => {
			const bounds = this.map.getBounds().toBBoxString();

			if (this.props.mode !== 'search') return;
			const {bloodTypes} = this.props;
			const query = {bb: bounds};
			if (bloodTypes && bloodTypes.size) {
				query.type = [...bloodTypes];
			}
			this.socket.emit('search', query);
			cb(query);
		};

		this.refreshMap = () => {
			this.map.off('moveend', this.search);
			this.map.once('moveend', () => {
				this.search(null, () => {}); // do nothing first time, (no url update) as 1st moveend is often a result of locationfound
				this.map.on('moveend', this.search); // now also update url
			});
			this.search(null, () => {});

			const sp = new URLSearchParams(location.search); // polyfill.io it
			const bb = (sp.get('bb') || '').split(',');
			if (bb.length === 4) {
				this.map.flyToBounds([[bb[1], bb[0]], [bb[3], bb[2]]]);
			} else {
				this.map.locate({setView: true, maxZoom: 16}); // use navigator.geolocation API
			}
		};
		
		this.refreshMap();

		window.addEventListener('popstate', this.refreshMap);

		this.searchResults = L.layerGroup().addTo(this.map);
		this.donors = new Map();

		this.bloodTypes = this.props.bloodTypes; // (1)

		const addDonor = r => {
			if (this.donors.has(r._id)) {
				const donor = this.donors.get(r._id);
				if (r.updated === donor.updated) {
					return; // skip it, since nothing changed
				}
				// else remove marker, so popup data gets updated
				this.map.removeLayer(donor.marker);

				// todo we could just .setContent and .setLatLng if data changed
			}
			const marker = L.marker([r.coords[1], r.coords[0]]).bindPopup(DonorTemplate(r)); // todo use L.popup and wrap event handler with it to lighten this module
			this.searchResults.addLayer(marker);

			const popupOpenListener = () => {
				const popup = marker.getPopup().getElement().querySelector('.donor');
				const emailElt = popup.querySelector('.email');
				const telElt = popup.querySelector('.tel');

				const emailListener = e => {
					fetchJSON(`/details/${r._id}/email`, {method: 'POST'})
						.then(email => {
							r.email = email;
							marker.getPopup().setContent(DonorTemplate(r));
							const telElt = marker.getPopup().getElement().querySelector('.tel');
							telElt.onclick = telListener;
						});
				};
				const telListener = e => {
					fetchJSON(`/details/${r._id}/tel`, {method: 'POST'})
						.then(tel => {
							r.tel = tel;
							marker.getPopup().setContent(DonorTemplate(r));
							const emailElt = marker.getPopup().getElement().querySelector('.email');
							emailElt.onclick = emailListener;
						});
				};

				if (emailElt.href !== 'mailto:'+r.email) {
					emailElt.onclick = emailListener;
				}
				if (telElt.href !== 'tel:'+r.tel) {
					telElt.onclick = telListener;
				}
			};

			marker.on('popupopen', popupOpenListener);

			r.marker = marker;
			this.donors.set(r._id, r);
		};

		this.socket.on('search', results => {
			results.forEach(r => {
				addDonor(r);
			});
		});

		this.socket.on('details new', data => {
			this.props.pushNotification(data);
			addDonor(data);
		});

		this.socket.on('details delete', _id => {
			if (this.donors.has(_id)) {
				this.map.removeLayer(this.donors.get(_id).marker);
			}
		});

		esri.basemapLayer('Streets').addTo(this.map);

		const searchControl = geocoding.geosearch().addTo(this.map);

		const geocodeService = geocoding.geocodeService();

		this.geocodeResults = L.layerGroup().addTo(this.map);

		this.map.on('click', e => {
			this.props.onClick();

			if (this.props.mode === 'post') {

				geocodeService.reverse().latlng(e.latlng).run((error, result) => {
					this.geocodeResults.clearLayers();

					const marker = L.marker(result.latlng).bindPopup(PostTemplate(result.address.Match_addr));
					this.geocodeResults.addLayer(marker);
					marker.openPopup();

					const form = marker.getPopup().getElement().querySelector('form');

					form.addEventListener('submit', e => {
						e.preventDefault();
						const type = form.querySelector('[name=type]:checked');
						const rh = form.querySelector('[name=rh]:checked');
						const postData = {
							address: result.address.Match_addr,
							lng: result.latlng.lng,
							lat: result.latlng.lat,
							firstName: form.firstName.value,
							lastName: form.lastName.value,
							email: form.email.value,
							tel: form.tel.value,
							type: `${type ? type.id.slice(2) : ''}${rh ? (rh.id==='rh1' ? '-' : '+') : ''}`,
						};
						fetchJSON('/details', {method: 'POST', body: postData})
							.then(r => {
								this.map.closePopup();
								this.geocodeResults.clearLayers();
								this.props.onSubmit(r);
							})
							.catch(e => {
								console.log(e);
								form.querySelector('.error').innerText = e.message;
							});
					});
				});

			}
	
		});
	}

	render() {
		return (
			<div id="map"></div>
		);
	}
}

const mapStateToProps = ({mode, bloodTypes}, ownProps) => ({mode, bloodTypes});

const mapDispatchToProps = (dispatch, ownProps) => ({
	onClick: e => {
		dispatch({type: 'closeSidebar'});
	},
	onSearch: query => {
		dispatch({type: 'search', query});
	},
	onSubmit: data => {
		dispatch({type: 'showSuccessPopup', data});
		dispatch({type: 'toggleMode', mode: 'search'});
	},
	pushNotification: data => {
		dispatch({type: 'addNotification', data});
		setTimeout(() => {
			dispatch({type: 'clearNotification', _id: data._id});
		}, 5000);
	}
});

const DonorMap = connect(mapStateToProps, mapDispatchToProps)(DonorMapView);

export default DonorMap;