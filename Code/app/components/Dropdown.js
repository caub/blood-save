import React from 'react';
import styled from 'styled-components';

// a Dropdown component must contain an element with className .dropdown-content
// DropdownContent can be used

// lowercase tag fails, todo investigate
export const DropdownContent = ({tag: Tag = 'div', ...props}) => (
	<Tag {...props} className={'dropdown-content ' + (props.className||'')}>
		{props.children}
	</Tag>
);

// todo adjust position below, depending on props.rtl, props.btt and available space in viewport
const arrowSize = 8;
const Div = styled.div`
	position: relative;

	.dropdown-content {
		position: absolute;
		z-index: 2;
		margin-top: 8px;
		box-shadow: 0 0 10px rgba(0,0,0,.075);
		border: 1px solid #eaeaea;
		border-radius: ${arrowSize}px;
		background-color: #fff;
		top: 125%;
		bottom: auto;
		right: 5px;
		left: auto;
		${props => props.active ? '' : 'display: none;'}

		&::after, &::before {
			content: " ";
			bottom: 100%;
			right: ${arrowSize*4}px;
			border: solid transparent;
			height: 0;
			width: 0;
			position: absolute;
			pointer-events: none;
		}

		&::after {
			border-color: rgba(0, 0, 0, 0);
			border-bottom-color: white;
			border-width: ${arrowSize}px;
			margin-left: -${arrowSize}px;
		}
		&::before {
			border-color: rgba(0, 0, 0, 0);
			border-bottom-color: #eaeaea;
			border-width: ${arrowSize+1}px;
			margin-left: -${arrowSize+1}px;
		}
	}
`;

export default class Dropdown extends React.PureComponent {

	constructor(props){
		super(props);

		this.state = {
			active: false
		};

		this.onToggle = e  => {
			const dropdown = this.dropdown;

			if (e === undefined) { // toggle
				this.toggleListeners(!this.state.active);
				this.setState({active: !this.state.active});
				return;
			}

			// click out or forceClose -> close
			if ((e.type === 'keydown' && (e.key === 'Escape' || e.key === 'Tab')) || dropdown && !dropdown.contains(e.target) || e === false) {
				this.toggleListeners(false);
				this.setState({active: false});
				return;
			}

			// toggle if click in dropdown buttons (anything but content)
			const content = dropdown.querySelector('.dropdown-content');
			if (!content || !content.contains(e.target)) {// toggle
				this.toggleListeners(!this.state.active);
				this.setState({active: !this.state.active});
			}
		};

		this.toggleListeners = force => { // todo add a resize handler
			if (force){
				document.addEventListener('mousedown', this.onToggle);
				document.addEventListener('keydown', this.onToggle);
			} else {
				document.removeEventListener('mousedown', this.onToggle);
				document.removeEventListener('keydown', this.onToggle);    
			}
		};

	}

	componentWillUnmount() {
		this.toggleListeners(false);
	}


	render() {
		
		const {active} = this.state;

		return (
			<Div 
				innerRef={el => this.dropdown = el}
				className={'dropdown ' + this.props.className}
				onMouseDown={this.onToggle}
				active={active}
			>
				{
					this.props.children({
						active,
						toggle: this.onToggle
					})
				}
			</Div>
		);
	}
}


Dropdown.defaultProps = {
	className: ''
	// rtl: false,
	// btt: false
};