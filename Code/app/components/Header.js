import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import BloodTypeFilter from './BloodTypeFilter';

const Div = styled.div`
	height: 3.2em;
	display: flex;
	align-items: center;
	background: rgba(0,0,0,.5);
	position: relative;
	z-index: 1001;

	.menu {
		margin: 2px 8px;
		transition: all .2s ease-in-out;
		position: absolute;
	}
	.menu::before {
		content: "${props => props.active ? '×': '☰'}";
		font-size: ${props => props.active ? '3em': '2em'};
		color: #e66;
	}
	.logo {
		color: #eee;
		margin: 0 auto;
		padding-right: 7em;
		@media (max-width: 800px) {
			margin: initial;
			margin-left: 2em;
		}
	}
	.logo::after {
		content: "Blood Save";
		@media (max-width: 800px) {
			content: "Save";
			color: #ff8a7b;
		}
	}
	.actions {
		position: absolute;
		right: 3px;
		display: flex;
		align-items: center;
		width: 330px;
		@media (max-width: 700px) {
			width: 215px;
		}
	}
	.btn {
		margin: 5px;
		user-select: none;
	}
	.btn:first-child {
		@media (max-width: 700px) {
			display: none;
		}
	}
`;

const HeaderView = ({sidebarActive, types, mode, onMenu, onModeChange}) => (
	<Div active={sidebarActive}>
		<button className="menu" onClick={onMenu} />
		<h2 className="logo small"></h2>
		<div className="actions">
			<button className={'btn ' + (mode === 'post' ? 'active' : '')} 
				onClick={() => onModeChange('post')} 
				title="Click on the map to create your donor submission"
			>Donate</button>
			<button className={'btn ' + (mode === 'post' ? '' : 'active')} 
				onClick={() => onModeChange('search')} 
				title="Search donor submissions in the map"
			>Search</button>
			<BloodTypeFilter />
		</div>
	</Div>
);

const mapStateToProps = (state, ownProps) => state;

const mapDispatchToProps = (dispatch, ownProps) => ({
	onMenu: e => {
		dispatch({type: 'toggleSidebar'});
	},
	onModeChange: mode => {
		dispatch({type: 'toggleMode', mode});
	}
});

const Header = connect(mapStateToProps, mapDispatchToProps)(HeaderView);

export default Header;