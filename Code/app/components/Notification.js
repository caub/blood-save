import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';

const Div = styled.div`
	position: absolute;
	z-index: 1002;
	bottom: 25px;
	right: 8px;
	width: 300px;
	list-style: none;
	margin: 0;
	border-radius: 4px;

	ul {
		overflow: hidden;
		max-height: ${props => props.active ? '400px': '0'};
	}
	li {
		padding: 15px 0 15px 20px;
		background: rgba(255,255,255,.8);
		transition: all .3s linear;
		overflow: hidden;
	}
	li.hide {
		opacity: 0;

	}
	span {
		display: inline-block;
		margin: 0 10px;
		vertical-align: middle;
	}
	.type {
		color: #c33;
	}

	.address {
		margin: 0;
		overflow: hidden;
		max-width: 220px;
		text-overflow: ellipsis;
	}

	button {
		position: absolute;
		top: -25px;
		right: -5px;
	}
	button::before {
		content: "${props => props.active ? '×': '^'}";
		font-size: ${props => props.active ? '1.8em': '2em'};
		transition: all .2s ease-in-out;
	}
`;

export const NotificationView = ({notifications = [], notificationActive = true, onToggle}) => (
	<Div active={notificationActive}>

		<button onClick={onToggle} title={(notificationActive ? 'Hide' : 'Show') + ' notifications'} />

		<ul>
			{notifications.map(data => (
				<li key={data._id} className={data.expired ? 'hide' : ''}>
					<span className="address">{data.address}</span>
					<span className="type">{data.type}</span>
				</li>
			))}
		</ul>

	</Div>
);

const mapStateToProps = (state, ownProps) => state; // todo tell react-redux to default to that

const mapDispatchToProps = (dispatch, ownProps) => ({
	onToggle: e => {
		dispatch({type: 'toggleNotifications'});
	}
});


const Notification = connect(mapStateToProps, mapDispatchToProps)(NotificationView);

export default Notification;