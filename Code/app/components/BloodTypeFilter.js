import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import Dropdown, {DropdownContent} from './Dropdown';
import {bloodTypesToString} from '../utils';

const Button = styled.button`
	font-size: 1em;
	line-height: 1.3em;
	color: ${props => props.disabled ? 'rgba(200,200,200,.5)' : '#eee'};
	width: 125px;
	pointer-events: ${props => props.disabled ? 'none' : 'auto'}
`;

const Content = styled(DropdownContent)`
	display: flex;
	align-items: center;

	input[type=checkbox] {
		margin: 0 10px 0 0;
	}
	label {
		display: flex;
		align-items: center;
		padding: 8px 25px;
		line-height: 40px;
		cursor: pointer;
		border: 1px solid rgba(90,90,90,.2);
	}
`;

const BloodTypeFilterView = ({bloodTypes = new Set(), mode, onFilterBloodType}) => (
	<Dropdown>{
		// a Dropdow children is a function with props as arg
		// props.active if the Dropdown is opened
		// props.toggle is a function, call it with false to close the dropdown, with no arg to toggle active
		// by default you don't have to do anything, a click in the dropdown not in the Content toggles, a click outside dropdown or Escape/Tab closes
		props => [
			<Button key="button" disabled={mode === 'post'}>{bloodTypes.size ? bloodTypesToString(bloodTypes) : 'any blood type'}</Button>,
			mode === 'post' ? null : <Content key="content">
				{
					['O', 'A', 'B', 'AB'].map(group => (
						<div key={group}>
							<label>
								<input type="checkbox" value={group+'-'} onChange={e => onFilterBloodType(group+'-', e.target.checked)}/>
								<span>{group+'-'}</span>
							</label>
							<label>
								<input type="checkbox" value={group+'+'} onChange={e => onFilterBloodType(group+'+', e.target.checked)}/>
								<span>{group+'+'}</span>
							</label>
						</div>
					))
				}
			</Content>
		]
	}
	</Dropdown>
);

const mapStateToProps = (state, ownProps) => state;

const mapDispatchToProps = (dispatch, ownProps) => ({
	onFilterBloodType: (bloodType, checked) => {
		dispatch({type: 'filterBloodType', bloodType, checked});
	}
});

const BloodTypeFilter = connect(mapStateToProps, mapDispatchToProps)(BloodTypeFilterView);

export default BloodTypeFilter;