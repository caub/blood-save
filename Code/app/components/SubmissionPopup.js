import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import {origin} from '../utils';

const Div = styled.div`
	position: absolute;
	background: rgba(0,0,0,.8);
	z-index: ${props => props.active ? 1003 : -1};
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	display: flex;
	align-items: center;
	justify-content: center;

	a {
		text-decoration: underline;
	}
	a::after {
		content: "↗";
	}

	> div {
		background: #eee;
		width: 400px;
		height: 300px;
		border-radius: 8px;
		border: 2px solid #eaa;
		padding: 8px 15px;
	}

	transition: transform .3s linear;
`;

const SubmissionPopupView = ({donorData = {}, onClose}) => (
	<Div active={donorData._id} className="popup" onClick={onClose}>
		<div>
			<h3>Success!</h3>
			<p>Your <a href={origin + '/details/' + donorData.privId} target="_blank">profile</a> is available</p>
			<pre>{`
name: ${donorData.firstName} ${donorData.lastName}
address: ${donorData.address}
email: ${donorData.email}
tel: ${donorData.telPrefix||''} ${donorData.tel}
ip: ${donorData.ip}
coords: ${donorData.coords}
blood type: ${donorData.type}
`}
			</pre>
		</div>
	</Div>
);

const mapStateToProps = (state, ownProps) => state;

const mapDispatchToProps = (dispatch, ownProps) => ({
	onClose: e => {
		if (e.target.matches('.popup')) {
			dispatch({type: 'closeSuccessPopup'});
		}
	}

});

const SubmissionPopup = connect(mapStateToProps, mapDispatchToProps)(SubmissionPopupView);

export default SubmissionPopup;