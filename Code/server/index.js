const path = require('path');
const express = require('express');
const serveStatic = require('serve-static');
const MongoClient = require('mongodb');
const HttpError = require('./http-error');
const {DB_URL} = require('./config');
const {corsMiddleware, httpsMiddleware, reactEngine} = require('./utils');

const isProd = process.env.NODE_ENV === 'production';
const staticOpts = {extensions: ['html'], maxAge: isProd ? '1d' : undefined};

// JSX for views folder
require('babel-register')({
	only: /views/,
	presets: ['react'], // not needed with .babelrc
	cache: isProd
});

const app = express()
	.disable('x-powered-by')
	.use(httpsMiddleware)
	.use(require('compression')())
	.set('views', __dirname + '/views')
	.set('view engine', 'js')
	.engine('js', reactEngine)
	.use('/', serveStatic(path.join(__dirname, '..', '..', 'dist'), staticOpts))
	.use(corsMiddleware);

const server = app.listen(process.env.PORT || 3000);

console.log('listen on', process.env.PORT || 3000, 'DB:', DB_URL);

module.exports = server;

// APIs init:
MongoClient.connect(DB_URL)
	.then(async db => {

		const User = await require('./models/user')(db, {init: true});
		const IO = require('socket.io')(server);

		// plug the RESTful http API
		app.use('/details', require('./rest-api')(IO, User));

		// plug the socket-io "API"
		require('./socket-api')(IO, User);

		// 404
		app.use((req, res, next) => {
			const error = new HttpError({status: 404, message: `page ${req.url} not found`});
			res.status(error.status);
			res.format({
				html() {
					res.render('Error', {message: error.message});
				},
				default() {
					res.json(error);
				}
			});
		});

		// error handler, last middleware
		app.use((err, req, res, next) => {
			const error = new HttpError(err);
			res.status(error.status || (error.message ? 500 : 404));

			res.format({
				html() {
					res.render('Error', {message: error.message});
				},
				default() {
					res.json(error);
				}
			});
		});
	});
