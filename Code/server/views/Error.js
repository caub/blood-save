const React = require('react');
const Layout = require('./Layout');

const Error = ({message = ''} = {}) => (
	<Layout>
		<main className="error">
			<h2>Oops</h2>
			<p>{message}</p>
		</main>
	</Layout>
);

module.exports = Error;