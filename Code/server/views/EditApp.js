const React = require('react');
const {debounce, ALL_TYPES} = require('../utils');

const toJson = r => (/^application\/json/.test(r.headers.get('Content-Type')) ? r.json() : r.text())
	.then(d => Promise[r.status>=400?'reject':'resolve'](d));

export const fetchJSON = (url, {method = 'GET', body}={}) => fetch(url, {
	method,
	body: body && JSON.stringify(body), 
	headers: {'Content-Type': 'application/json', Accept:'*/json'}
}).then(toJson);

module.exports = class EditApp extends React.PureComponent {

	constructor(props) {
		super(props);

		this.state = {...props};

		this.onSubmit = e => {
			e.preventDefault();
			const form = e.target;
			fetchJSON(location.href, {
				method: 'PATCH',
				body: {
					firstName: form.firstName.value,
					lastName: form.lastName.value,
					email: form.email.value,
					tel: form.tel.value,
					type: form.type.value
				}
			})
				.then(r => {
					this.setState({...r, edit: false});
				})
				.catch(e => {
					this.setState({error: e.message});
				});
		};

		this.onDelete = e => {
			fetchJSON(location.href, { // todo allow to undo within 15s
				method: 'PATCH',
				body: {trashed: true}
			})
				.then(r => {
					this.setState({...r, edit: false});
				})
				.catch(e => {
					this.setState({error: e.message});
				});
		};

		this.undoTrashed = e => {
			fetchJSON(location.href, { // todo allow to undo within 15s
				method: 'PATCH',
				body: {trashed: false}
			})
				.then(r => {
					this.setState({...r, edit: false});
				})
				.catch(e => {
					this.setState({error: e.message});
				});
		};
	}

	render() {
		const {_id, ip, coords, address, firstName, lastName, email, telPrefix, tel, type, edit} = this.state;
		return (
			<div>
				<header>
					<h2>
						<a href="https://blood-save.herokuapp.com" className="logo">Blood Save</a>
					</h2>
				</header>
				<main>
					<h3><span>Donor submission</span></h3>

					{edit ? (
						<form className="submission" onSubmit={this.onSubmit} method="PATCH">
							<div><span>ID</span><code>{_id}</code></div>
							<div><span>Address</span><span className="address">{address}</span></div>
							<div><span>IP</span><code>{ip}</code></div>
							<div>
								<span>Name</span>
								<input defaultValue={firstName} placeholder="first name" name="firstName" type="text" required />
								<input defaultValue={lastName} placeholder="last name" name="lastName" type="text" required />
							</div>
							<div>
								<label htmlFor="email">Email</label>
								<input id="email" defaultValue={email} placeholder="email" name="email" type="email" required />
							</div>
							<div>
								<label htmlFor="tel">Phone</label>
								<input id="tel" 
									defaultValue={(telPrefix ? telPrefix + ' ' : '') + tel}
									placeholder="phone number" name="tel" type="tel" required />
							</div>
							<div>
								<label htmlFor="type">Blood type</label>
								<select id="type" 
									defaultValue={type}
									name="type" required
								>
									{ALL_TYPES.map(t => <option key={t}>{t}</option>)}
								</select>
							</div>
							<div className="footer">
								<input type="submit" value="Save" />
							</div>
							<div className="footer">
								<input className="delete" type="button" value="Delete" onClick={this.onDelete} />
							</div>
							<div className="error">{this.state.error}</div>
							<button onClick={() => this.setState({edit: false})} title="Go back" className="prev"></button>
						</form>
					) : (
						<div className="submission">

							<div><span>ID</span><code>{_id}</code></div>
							<div><span>Address</span><span className="address">{address}</span></div>
							<div><span>IP</span><code>{ip}</code></div>
							<div>
								<span>Name</span>
								<span>{firstName}</span>
								<span>{lastName}</span>
							</div>
							<div>
								<span>Email</span>
								<span>{email}</span>
							</div>
							<div>
								<span>Phone</span>
								<span>{(telPrefix ? telPrefix + ' ' : '') + tel}</span>
							</div>
							<div>
								<span>Blood type</span>
								<span>{type}</span>
							</div>

							{this.state.trashed && (
								<div className="footer">
									<div>
										<p>This submission is no longer visible</p>
										<p>It will be removed in 2 days</p>
									</div>
									<button type="button" onClick={this.undoTrashed} title="Undo">Undo</button>
								</div>
							)}
							<button onClick={() => this.setState({edit: true})} title="Edit" className="next"></button>
							
						</div>
					)}
				</main>
			</div>
		);
	}
};