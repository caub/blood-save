const React = require('react');

const Layout = ({children}) => (
	<html lang="en">
		<head>
			<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />
			<link rel="shortcut icon" href="data:image/gif;base64," type="image/x-icon"/>
			<title>Blood Save</title>
			<link rel="stylesheet" type="text/css" href="/edit.css" />
		</head>

		<body>
			{children}
		</body>

	</html>
);

module.exports = Layout;