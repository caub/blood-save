const React = require('react');
const ReactDOMServer = require('react-dom/server');
const Layout = require('./Layout');

const EditApp = require('./EditApp');

const Details = data => (
	<Layout>
		<div id="root"
			data-props={encodeURIComponent(JSON.stringify(data))}
			dangerouslySetInnerHTML={{__html: ReactDOMServer.renderToString(<EditApp {...data}/>)}}
		/>

		<script src="/edit.js"></script>
	</Layout>
);

module.exports = Details;