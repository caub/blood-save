const {createElement} = require('react');
const {renderToStaticMarkup} = require('react-dom/server');
const {ORIGINS_RE} = require('./config');
const countries = require('country-data/data/countries.json').filter(o => o.status !== 'deleted');

const countryCodes = new Map(countries.map(o => [o.alpha2, o])); // countries indexed by 2-letter code

exports.countryCodes = countryCodes;

exports.ALL_TYPES = ['O', 'O-', 'O+', 'A', 'A-', 'A+', 'B', 'B-', 'B+', 'AB', 'AB-', 'AB+'];

// a telephone number can optionally contain a country code prefix, extracts it
exports.parseTelPrefix = s => {
	if (typeof s !== 'string') {
		return {};
	}
	const m = s.trim().match(/^(\+|00)\d+/);
	if (m) {
		return {telPrefix: m[0].replace(/^00/, '+'), tel: s.slice(m[0].length).trim()};
	}
	return {tel: s};
};

exports.parseCoords = ({lng: lng_, lat: lat_}) => {
	const lng = +lng_, lat = +lat_;
	return !isNaN(lng) && !isNaN(lat) && lng >= -180 && lng <= 180 && lat >= -90 && lat <= 90 ? {lng, lat} : {};
};

exports.corsMiddleware = (req, res, next) => { // CORS, todo https://socket.io/docs/server-api/#server-origins-value for socket security
	const origin = req.headers.origin, isAllowed = ORIGINS_RE.test(origin);
	res.setHeader('Access-Control-Allow-Origin', isAllowed ? origin : '*');
	res.setHeader('Access-Control-Allow-Credentials', isAllowed);
	
	if (req.method.toUpperCase() === 'OPTIONS') {
		res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
		res.setHeader('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
		res.setHeader('Access-Control-Max-Age', '86400');
		return res.status(204).end();
	}
	next();
};

exports.httpsMiddleware = (req, res, next) => { // redirect http -> https (except localhost)
	const proto = req.headers['x-forwarded-proto'] || '';
	const isHttps = req.secure || proto.startsWith('https') || /^localhost:/.test(req.headers.host);
	if (!isHttps && (req.method=='GET' || req.method=='HEAD')) {
		return res.redirect(308, 'https://' + req.headers.host + req.originalUrl);
	}
	next();
};

exports.debounce = (cb, delay) => {
	let timeout;
	return function(...a) {
		clearTimeout(timeout);
		timeout = setTimeout(() => {
			timeout = null;  
			cb.call(this, ...a);
		}, delay);
	};
};

exports.reactEngine = function reactEngine(filePath, options, callback) {
	const component = require(filePath);
	const markup = renderToStaticMarkup(createElement(component, options));
	callback(null, markup);
};