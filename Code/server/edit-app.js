const {createElement} = require('react');
const {hydrate} = require('react-dom');
const EditApp = require('./views/EditApp');

// entry point for webpack

const rootEl = document.getElementById('root'); // get root container

const data = JSON.parse(decodeURIComponent(rootEl.dataset.props));

hydrate(
	createElment(EditApp, data),
	rootEl
);