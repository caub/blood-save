const isTest = process.env.NODE_ENV === 'test';

exports.DB_URL = (process.env.DB_URL || 'mongodb://localhost:27017/blood_save') + (isTest ? '_test' : '');

exports.SESSION_SECRET = process.env.SESSION_SECRET || '1234';

// for CORS
exports.ORIGINS_RE = /\/$blood-save.herokuapp.com|[./]blood\.sv$|\/localhost:\d+$/; // localhost for dev