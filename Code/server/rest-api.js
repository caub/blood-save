const router = require('express').Router();

router.use(require('body-parser').json());
router.use(require('body-parser').urlencoded({extended: true}));

module.exports = (IO, User) => {

	// :privId is the long private ID, :id is the public short one
	
	router.get('/:privId', (req, res, next) => {
		User.show(req.params.privId)
			.then(r => {
				res.format({
					html() {
						res.render('Details', r);
					},
					default() {
						res.json(r);
					}
				});
			})
			.catch(next);
	});

	router.post('/:id/email', (req, res, next) => { // as a POST, because we increment a counter
		User.showEmail(req.params.id)
			.then(r => res.json(r))
			.catch(next);
	});
	router.post('/:id/tel', (req, res, next) => { // as a POST, because we increment a counter
		User.showTel(req.params.id)
			.then(r => res.json(r))
			.catch(next);
	});

	router.patch('/:privId', (req, res, next) => {
		const ip = req.header('x-forwarded-for') || req.client.remoteAddress;
		User.edit(req.params.privId, req.body, ip)
			.then(r => {
				// broadcast this donor data edit
				const {_id, coords, created, address, firstName, lastName, type, trashed} = r;
				if (trashed) {
					IO.emit('details delete', _id);
				} else {
					IO.emit('details new', {_id, coords, created, address, firstName, lastName, type});
				}

				res.json(r);
			})
			.catch(next);
	});

	router.delete('/:privId', (req, res, next) => {
		User.delete(req.params.privId)
			.then(r => {
				// broadcast this donor data deletion
				IO.emit('details delete', r._id);

				res.json(r);
			})
			.catch(next);
	});

	router.post('/', (req, res, next) => {
		const ip = req.header('x-forwarded-for') || req.client.remoteAddress;
		User.create(req.body, ip)
			.then(r => {
				// broadcast this new donor data
				const {_id, coords, created, address, firstName, lastName, type} = r;
				IO.emit('details new', {_id, coords, created, address, firstName, lastName, type});

				res.json(r);
			})
			.catch(next);
	});

	return router;
};