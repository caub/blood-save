// there used to be more things, moved mostly to rest-api
// only real-time and frequent (pull/push) requests are done here 

module.exports = (IO, User) => {
	
	IO.on('connection', socket => {
		// const ip = socket.handshake.headers['x-forwarded-for'] || socket.conn.remoteAddress; // for heroku
		// console.log('Client connected', ip);

		socket.on('search', data => {
			User.search(data)
				.then(r => socket.emit('search', r)) // return results
				.catch(e => socket.emit('app error', e));
		});

		// socket.on('disconnect', () => console.log('Client disconnected', ip));
	});
};