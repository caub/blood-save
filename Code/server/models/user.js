const shortid = require('shortid');
const uid = require('uid-safe').sync;
const HttpError = require('../http-error');
const {parseTelPrefix, parseCoords, ALL_TYPES} = require('../utils');

// email regexp https://www.w3.org/TR/html5/forms.html#e-mail-state-(type=email)
const EMAIL_RE =  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const TYPE_RE = /^(O|A|B|AB)[+-]?$/;

const SEARCH_PROJECTION = {coords: 1, created: 1, address: 1, firstName: 1, lastName: 1, type: 1};

// User fields types:
const validator = {
	_id: {$type: 'string'},
	privId: {$type: 'string'}, // private long Id
	email: {$regex: EMAIL_RE},
	// coords: {$type: 'array'}, // [lng, lat]
	'coords.0': {$type: 'number'},
	'coords.1': {$type: 'number'}, // todo {$lt: 90}...
	created: {$type: 'date'},
	address: {$type: 'string'},
	tel: {$type: 'string'},
	firstName: {$type: 'string'},
	lastName: {$type: 'string'},
	type: {$regex: TYPE_RE},
	trashed: {$type: 'bool'},

	// optional fields
	$and: [{
		$or: [{telPrefix: {$type: 'string'}}, {telPrefix: {$eq: null}}, {telPrefix: {$exists: false}}]
	}, {
		$or: [{updated: {$type: 'date'}}, {updated: {$eq: null}}, {updated: {$exists: false}}]
	}]
};


module.exports = async(db, opts) => {
	const collName = opts && opts.collectionName || 'users';
	let Users = db.collection(collName);
	
	if (opts && opts.init) {
		Users = await db.createCollection(collName, {validator}); // could use db.command({collMod: collName, validator}); if it exists
		await Users.createIndex({coords: '2dsphere'});
		await Users.createIndex({email: 1}, {unique:true, collation: {locale: 'en', strength: 2}});// unique case-insensitive
		await Users.createIndex({privId: 1}, {unique:true});
		await Users.createIndex({type: 1, trashed: 1});
	}

	/**
	 * Validates data, when present
	 */
	async function validateDonor({email, tel, firstName, lastName, type, address}, checkUniqueEmail) {
		if (type !== undefined && !ALL_TYPES.includes(type)) {
			throw new HttpError({status: 400, message: 'Invalid blood type'});
		}
		if (tel !== undefined && !tel) {
			throw new HttpError({status: 400, message: 'Invalid telephone number (tel)'});
		}
		if (lastName !== undefined && !firstName) {
			throw new HttpError({status: 400, message: 'Invalid firstName'});
		}
		if (lastName !== undefined && !lastName) {
			throw new HttpError({status: 400, message: 'Invalid lastName'});
		}
		if (address !== undefined && !address) {
			throw new HttpError({status: 400, message: 'Invalid address'});
		}
		if (email !== undefined && !EMAIL_RE.test(email)) {
			throw new HttpError({status: 400, message: 'Invalid email'});
		}
		if (checkUniqueEmail && email !== undefined) {
			const hasEmail = await Users.findOne({email});
			if (hasEmail) {
				throw new HttpError({status: 400, message: 'Email already exists, we will send you your submissions by email'});
			}
		}
	}


	return {
		/**
		 * lng, lat, radius are numbers
		 * type is an optional blood type filter, if it doesn't end with -+ rhesus, it checks both
		 */
		search({bb = '', type}) {
			const bloodType = typeof type !== 'string' ? {$in: Array.isArray(type) ? type : ALL_TYPES} : 
				/[+-]$/.test(type) ? type.toUpperCase() :
					{$regex: `^${type}[+-]?$`};

			const [lng1, lat1, lng2, lat2] = bb.split(/[,+\s]/g).map(x => +x);
			if (isNaN(lng1) || isNaN(lat1) || isNaN(lng2) || isNaN(lat2)) {
				return Promise.reject(new HttpError({status: 400, message: 'Invalid input bb: boundingbox needs 4 parts'}));
			}

			return Users.find({
				coords: {
					$geoWithin: {
						$box: [
							[lng1, lat1],
							[lng2, lat2]
						]
					}
				},
				type: bloodType,
				trashed: false
			}, SEARCH_PROJECTION).toArray();
			
			// todo: how to paginate? $min/maxDistance? depends how many donors we expect
		},

		showEmail(_id) {
			return Users.findOneAndUpdate({_id},
				{$inc: {emailViews: 1}},
				{projection: {email: 1}}
			).then(({value}) => {
				if (!value) {
					throw new HttpError({status: 404, message: `Not found ${_id}`});
				}
				return value.email;
			});
		},

		showTel(_id) {
			return Users.findOneAndUpdate({_id},
				{$inc: {telViews: 1}},
				{projection: {telPrefix:1, tel: 1}}
			).then(({value}) => {
				if (!value) {
					throw new HttpError({status: 404, message: `Not found ${_id}`});
				}
				return `${value.telPrefix ? value.telPrefix+' ' : ''}${value.tel}`;
			});
		},

		show(privId) {
			return Users.findOne({privId})
				.then(value => {
					if (!value) {
						throw new HttpError({status: 404, message: `Not found ${privId}`});
					}
					return value;
				});
		},

		async edit(privId, data, ip) {
			await validateDonor(data);

			const patchData = {ip, ...parseTelPrefix(data.tel)};

			['firstName', 'lastName', 'email', 'type'].forEach(s => {
				if (data[s]) {
					patchData[s] = data[s];
				}
			});
			if (data.trashed === true || data.trashed === false) {
				patchData.trashed = data.trashed;
			}
			// don't edit address, lng/lat here, it must be consistent and done from app
			// if (data.lng!==undefined && data.lat!==undefined) {
			// 	const {lng, lat} = parseCoords({lng: data.lng, lat: data.lat});
			// 	patchData.coords = [lng, lat];
			// }
			if (Object.keys(patchData).length > 1) {
				patchData.updated = new Date();
			}

			return Users.findOneAndUpdate({privId}, {$set: patchData}, {returnOriginal: false})
				.then(({value}) => value);
		},

		delete(privId) {
			return Users.deleteOne({privId})
				.then(r => r.result);
		},


		/**
      required: email, tel, type, and either (lng,lat or street)
		 */
		async create(o, ip) {
			const firstName = String(o.firstName || '').trim();
			const lastName = String(o.lastName || '').trim();
			const email = String(o.email || '').trim();
			const tel = String(o.tel || '').trim();
			const type = String(o.type || '').trim();
			const address = String(o.address || '').trim();
			await validateDonor({firstName, lastName, email, tel, type, address}, true);

			const {lng, lat} = parseCoords({lng: o.lng, lat: o.lat});

			if (lng===undefined || lat===undefined) {
				throw new HttpError({status: 400, message: `Missing {lat, lng}`});
			}

			try {
				const userData = {
					_id: shortid(),
					privId: uid(30),
					created: new Date(),
					trashed: false,
					firstName,
					lastName,
					coords: [lng, lat],
					email,
					type,
					address,
					ip,
					...parseTelPrefix(tel)
				};
				const {ops: [user]} = await Users.insertOne(userData);
				return user;

			} catch(e) {
				throw new HttpError({status: 400, message: e.message}); // todo format error message
			}
		}
	};
};