
:heavy_check_mark: Project organization  
:heavy_check_mark: Completeness  
:heavy_check_mark: Demo presentation  

:x: Low design documentation.  
:x: Modularity  
:x: Maintainability.  
:x: The tests could not be validated due to an unhandled error.  
:x: Code issues: function with many if/else statements are hard to read, test and maintain, nested blocks too deeply, empty blocks.  