In brief:

Make an app to connect patients and blood donors volunteers

It should be based on a Map, Donors can add their info and get a unique private link for their submission. Patients can search.

## Requirements
1. A single page architecture of data driven app, using MEAN stack.
2. Feel free to chose between Angular 2 or REACT as your front end framework.
3. Index page should mainly contain a map using ArcGIS
4. The postings should be real time, using sockets via socket.io
5. MongoDb NoSQL database should be used.
6. Unit tests should be written, where necessary.

## Delivery
- README.txt -> Contains instructions and feedback.
- Design.doc -> Contains your description of solution design and technologies.
- Demo.txt -> Contains link to live version on application.
- Code Folder -> Contains your solution.
- Tests Folder -> Contains all tests.
- Video folder -> Contains your video showing your working solution.